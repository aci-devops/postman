import csv

# 1. Abra o arquivo de texto (substitua 'seu_arquivo.txt' pelo caminho para o seu arquivo)
nome_arquivo = 'vlans.txt'

try:
    with open(nome_arquivo, 'r') as arquivo:
        # 2. Leia o conteúdo do arquivo
        conteudo = arquivo.read()

        # 3. Separe os números usando a vírgula como delimitador
        numeros = conteudo.split(',')

        # Inicialize as listas para armazenar os dados
        dados = []

        # 4. Processar os números
        for numero in numeros:
            if '-' in numero:
                inicio, fim = map(int, numero.split('-'))
                numeros_expandidos = range(inicio, fim + 1)
            else:
                numero_int = int(numero.strip())
                numeros_expandidos = [numero_int]

            # Para cada número expandido, adicione uma linha de dados
            for numero in numeros_expandidos:
                dados.append({
                    'TENANT': 'TN_PRODEMGE',
                    'ANP': 'APP-PRODEMGE',
                    'EPG': 'vlan' + str(numero) + '_epg',
                    'LEAFID1': '101',
                    'LEAFID2': '102',
                    'VPC_INT_POLGROUP': 'CHIMEARA',
                    'VLANID': numero
                })

        # 5. Salvar os dados em um arquivo CSV
        nome_arquivo_csv = 'Static Path vPC.csv'
        with open(nome_arquivo_csv, 'w', newline='') as arquivo_csv:
            campos = ['TENANT', 'ANP', 'EPG', 'LEAFID1', 'LEAFID2', 'VPC_INT_POLGROUP', 'VLANID']
            escritor_csv = csv.DictWriter(arquivo_csv, fieldnames=campos)
            escritor_csv.writeheader()
            escritor_csv.writerows(dados)

        print(f'Dados salvos em {nome_arquivo_csv}.')

except FileNotFoundError:
    print(f"Arquivo '{nome_arquivo}' não encontrado.")
except Exception as e:
    print(f"Ocorreu um erro: {e}")
